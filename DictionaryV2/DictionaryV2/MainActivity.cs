﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Preferences;
using Newtonsoft.Json;

namespace DictionaryV2
{
    [Activity(Label = "DictionaryV2", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private EditText text;
        private Button search;
        private Button menu;

        private ListView wordView;
        private MemoryObject memObj;
        private Boolean DictionariesArePresent;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            memObj = new MemoryObject();
            Console.WriteLine("CREATE MAIN ACTI");
            text = (EditText)FindViewById(Resource.Id.text);
            search = (Button)FindViewById(Resource.Id.search);
            wordView = (ListView)FindViewById(Resource.Id.words);
            menu = (Button)FindViewById(Resource.Id.menu);

            DictionariesArePresent = false;


            menu.Click += (s, arg) => {
                PopupMenu pMenu = new PopupMenu(this, menu);
                pMenu.Inflate(Resource.Layout.Menu);
                pMenu.Show();
                pMenu.MenuItemClick += (object sender, PopupMenu.MenuItemClickEventArgs e) => ClickMenu(sender, e);
            };


            search.Click += (sender, EventArgs) => searchWord();
           

        }

        private void ClickMenu(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            string title = e.Item.TitleCondensedFormatted.ToString();
            if(title.Equals("Select dictionaries"))
            {
                setDictionaryView();
            }else if (title.Equals("Share"))
            {
                // TODO
            }
        }

        private void setDictionaryView()
        {
            var intent = new Intent(this, typeof(DictionaryActivity));
            if(memObj.dictionaries != null)
            {
                var serialized = JsonConvert.SerializeObject(memObj);
                intent.PutExtra("MemObject", serialized);
            }
            StartActivity(intent);
        }

        private void searchWord()
        {
            string input = text.Text;
            Console.WriteLine("input: " + input);
            if (input != "")
            {
                var def = new com.aonaware.services.DictService();
                def.DefineAsync(input);
                def.DefineCompleted += DictRequestCompleted;
            }
        }

        private void DictRequestCompleted(Object sender, com.aonaware.services.DefineCompletedEventArgs e)
        {
            memObj.words.Clear();
            var definition = e.Result;
 

            foreach(var word in definition.Definitions)
            {
                Word wrd = new Word(word.WordDefinition, word.Dictionary.Name);
               
                if(DictionariesArePresent && getSelectedDictionaries().Contains(word.Dictionary.Name)  )
                {
                    memObj.words.Add(wrd);
                }
                else if (!DictionariesArePresent)
                {
                    memObj.words.Add(wrd);
                }
             
            }
        
            wordView.Adapter = new DefinitionAdapter(this, memObj.words);
        }



        protected override void OnResume()
        {
            Console.WriteLine("ONRESUME MAIN ACTI");
            ISharedPreferences preferences = PreferenceManager.GetDefaultSharedPreferences(this);
            if(preferences.GetString("MemObject", "") != "")
            {
                memObj = JsonConvert.DeserializeObject<MemoryObject>(preferences.GetString("MemObject", ""));
                Console.WriteLine("het aantal definities: " + memObj.words.Count);
                DictionariesArePresent = true;
            }

            var otherView = Intent.GetSerializableExtra("MemObject");
            if(otherView != null)
            {
                
                memObj = JsonConvert.DeserializeObject<MemoryObject>(otherView.ToString());
                if(memObj.dictionaries.Count > 0)
                {
                    Console.WriteLine("ik geraak hier + aantal dicts = " + memObj.dictionaries.Count);
                    text.Text = preferences.GetString("LastWord", "");
                    DictionariesArePresent = true;

                }
            }

            searchWord();
            base.OnResume();
        }

        private List<String> getSelectedDictionaries()
        {
            List<String> dicts = new List<String>();
            for(int i =0; memObj.dictionaries != null && i != memObj.dictionaries.Count; ++i)
            {
                if (memObj.dictionaries[i].isChecked)
                {
                    dicts.Add(memObj.dictionaries[i].dict);
                }
            }
            return dicts;
        }
         

        protected override void OnStop()
        {
            base.OnStop();
        }

        protected override void OnPause()
        {
            ISharedPreferences sharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            ISharedPreferencesEditor editor = sharedPreferences.Edit();
            editor.PutString("MemObject", JsonConvert.SerializeObject(memObj));
            editor.PutString("LastWord", text.Text);
            editor.Apply();
            base.OnPause();
        }


        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}

