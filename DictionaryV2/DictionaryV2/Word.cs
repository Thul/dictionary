using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DictionaryV2
{
    [Serializable]
    class Word
    {
        public String description { get; set; }
        public String dictionary { get; set; }

        public Word(string description, string dictionary)
        {
            this.description = description;
            this.dictionary = dictionary;
        }
    }
}