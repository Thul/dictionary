using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Android.Preferences;

namespace DictionaryV2
{
    [Activity(Label = "DictionaryActivity")]
    public class DictionaryActivity : Activity
    {
        
        private Button menu;
        private MemoryObject memObj;
        private ListView DictionaryView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DictionaryView);
            memObj = new MemoryObject();
            memObj.dictionaries = new List<Dictionary>();
            DictionaryView = (ListView)FindViewById(Resource.Id.dicts);
            Console.WriteLine("CREATE");
            menu = (Button)FindViewById(Resource.Id.menu);

            menu.Click += (s, arg) => {
                PopupMenu pMenu = new PopupMenu(this, menu);
                pMenu.Inflate(Resource.Layout.Menu);
                pMenu.Show();
                pMenu.MenuItemClick += (object sender, PopupMenu.MenuItemClickEventArgs e) => ClickMenu(sender, e);
            };

            getDictionaries();

        }

        private void getDictionaries()
        {
            var def = new com.aonaware.services.DictService();
            def.DictionaryListAsync();
            def.DictionaryListCompleted += DictListCompleted;
        }

        private void DictListCompleted(Object sender, com.aonaware.services.DictionaryListCompletedEventArgs e)
        {
            var _dictionaries = e.Result;
 
            memObj.dictionaries.Clear();
            ISharedPreferences preferences = PreferenceManager.GetDefaultSharedPreferences(this);
          
            if (preferences.GetString("MemObject", "") != "") memObj = JsonConvert.DeserializeObject<MemoryObject>(preferences.GetString("MemObject", ""));
            var otherView = Intent.GetSerializableExtra("MemObject");
            bool serialized = otherView != null;
            if (serialized) memObj = JsonConvert.DeserializeObject<MemoryObject>(otherView.ToString()); 
            
            List<string> dicts = new List<string>();
 
            var dictionariesExist = memObj.dictionaries.Count > 0;
            
            
            for(int i = 0; i != _dictionaries.Length; ++i)
            {
                dicts.Add(_dictionaries[i].Name);
                if (dictionariesExist)
                {
                    var dictionary = new Dictionary(memObj.dictionaries[i].id, memObj.dictionaries[i].dict, memObj.dictionaries[i].isChecked);
                    memObj.dictionaries.Add(dictionary);
                }
                else
                {
                   memObj.dictionaries.Add(new Dictionary(_dictionaries[i].Id, _dictionaries[i].Name, true));

                }
   
            }

            DictionaryView.Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItemMultipleChoice, dicts);
            DictionaryView.ChoiceMode = ChoiceMode.Multiple;
            DictionaryView.ItemClick += DictionaryItemClick;
        }

        private void DictionaryItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            for (int i = 0; i != DictionaryView.Count; ++i)
            {
                if (DictionaryView.IsItemChecked(i))
                {
                    memObj.dictionaries[i].isChecked = true;
                    continue;
                }
                memObj.dictionaries[i].isChecked= false;
            }
        }


        private void ClickMenu(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            string title = e.Item.TitleCondensedFormatted.ToString();
            if (title.Equals("Look up a word"))
            {
                setMainView();
            }
            else if (title.Equals("Share"))
            {
                // TODO
            }
        }


        private void setMainView()
        {
            var intent = new Intent(this, typeof(MainActivity));
            var serialized = JsonConvert.SerializeObject(memObj);
            intent.PutExtra("MemObject", serialized);
            StartActivity(intent);
        }

        protected override void OnPause()
        {
            ISharedPreferences sharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            ISharedPreferencesEditor editor = sharedPreferences.Edit();
            editor.PutString("MemObject", JsonConvert.SerializeObject(memObj));
            editor.Apply();
            base.OnPause();
        }

        protected override void OnResume()
        {

           
            ISharedPreferences preferences = PreferenceManager.GetDefaultSharedPreferences(this);
            if (preferences.GetString("MemObject", "") != "")
            {
                memObj = JsonConvert.DeserializeObject<MemoryObject>(preferences.GetString("MemObject", ""));
            }
            for (int j = 0; j != DictionaryView.Count; ++j)
            {
                DictionaryView.SetItemChecked(j, memObj.dictionaries[j].isChecked);
            }
            base.OnResume();
        }


    }
}